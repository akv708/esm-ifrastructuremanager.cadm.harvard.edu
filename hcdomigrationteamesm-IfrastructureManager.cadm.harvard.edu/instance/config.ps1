# Are you running in 32-bit mode?
#   (\SysWOW64\ = 32-bit mode)

if ($PSHOME -like "*SysWOW64*")
{
  Write-Warning "Restarting this script under 64-bit Windows PowerShell."

  # Restart this script under 64-bit Windows PowerShell.
  #   (\SysNative\ redirects to \System32\ for 64-bit mode)

  & (Join-Path ($PSHOME -replace "SysWOW64", "SysNative") powershell.exe) -File `
    (Join-Path $PSScriptRoot $MyInvocation.MyCommand) @args

  # Exit 32-bit script.

  Exit $LastExitCode
}

Try
{
    Import-Module C:\downloads\instance\functions.psm1

    Get-Environment-Variables
    
    If (-Not (Test-RegistryValue -Path 'HKLM:\SOFTWARE\Automation\' -Name 'BuildStarted' -Value 1))
    {
        #DOWNLOAD REMOTE SCRIPTS FROM S3
        Download-App-S3-Files "$Global:BuildScriptsS3BucketName" "$Global:S3BuildScriptsDir/" $Global:BuildScriptsDir\
    }

    $Global:BuildInitiatedFromUserData = $true

    #EXECUTE THE BUILD SCRIPT
    & ("$($Global:BuildScriptsDir)\pattern-build.ps1")
}
Catch
{
    Write-Log "Exception Message: $($_.Exception.Message)"
    Write-Log "Exception Item Name: $($_.Exception.ItemName)"
}