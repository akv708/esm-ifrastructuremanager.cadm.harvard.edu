# Environment agnostic overrides
#
# Variables declared here will overwrite values in defaults.ps1 for all
# environments. For example:

# IIS should not be installed if you are using a Custom AMI.  The custom AMI
# should have IIS pre-installed.
$Global:InstallIIS = $false
$Global:NessusGroupName = 'HUIT-WINDOWS'

$Global:Modules = "core",
                  "utils",
                  "middleware",
                  "supplemental"

$Global:ModuleCore = "eni.ps1",
                     "domainjoin.ps1",
                     "administratorgroups.ps1",
                     "sccm.ps1",
                     "scom.ps1",
                     "shavlik.ps1",
                     "splunk.ps1",
                     "crowdstrike.ps1",
                     "nessus.ps1",
                     "networker.ps1",
                     "symantec.ps1"

$Global:ModuleUtils = $null

$Global:ModuleSupplemental = $null
                             

$Global:VerboseLoggingForInstalls = $true

#Use this variable to specify additional Administrators to be added to the local machine.
$Global:ServerAdditionalAdministrators = "UNIVERSITY\uis-ESM-Admin-dgs"

# Environment specific overrides
#
# Use the block of each case to create environment specific overrides for
# values found in defaults.sh. If zero overrides are needed place a semi-colon, i.e.
# ';', in the body of the case to express a no-op.

Switch ($Global:Env) {
    "prod" 
    { 
        $Global:ServerHostName='UISWAPPS1000034'
        $Global:ServerOU='OU=ems-prod,OU=Amazon-AWS,OU=Cloud,OU=Servers,OU=Technology,OU=!Common,DC=university,DC=harvard,DC=edu'
        $Global:ShavlikPolicyName='Week4 5am'
        $Global:SplunkIndex='soc-wintel'
    }
}